const IO = run => ({
	run,
	ap: a => IO(() => run()(a.run())),
	chain: f => IO.of(f(run()).run()),
	map: f => IO.of(f(run())),
  inspect: () => `IO(${run.toString()})`,
  __type__: 'IO'
})

IO.of = x => IO(() => x)

module.exports = IO

const { stringify } = require('./utils')

const Some = x => ({
  map: (f) => Some(f(x)),
  chain: (f) => f(x),
  fold: (f) => f(x),
  ap: (e) => e.map(x),
  inspect: () => `Some(${stringify(x)})`,
  __type__: 'Some'
})

const None = () => ({
  map: (f) => None(),
  chain: (f) => None(),
  ap: (e) => None(),
  fold: (f) => null,
  inspect: () => `None()`,
  __type__: 'None'
})

const Maybe = (x) =>
  x !== null ? Some(x) : None()

Maybe.of = Some
Maybe.Some = Some
Maybe.None = None

module.exports = Maybe

const getType = (x) =>  {
  if(x && x.__type__) return x.__type__
  const t1 = Object.prototype.toString.apply(x)
    .replace('[object ', '')
    .replace(']', '')
  return x && t1 != x.constructor.name ? x.constructor.name : t1
}

const stringify = (x) =>
  /Array|Object/.test(getType(x)) ?
    JSON.stringify(x) :
    x.toString()

const once = (fn, output) => (...args) =>
  !output && (output = fn.apply(null, args))

const liftA2 = (fn, m1, m2) => 
  m1.map(fn).ap(m2)

const liftA3 = (fn, m1, m2, m3) =>
  m1.map(fn).ap(m2).ap(m3)

module.exports = {
  stringify: stringify,
  getType:   getType,
  liftA2:    liftA2,
  liftA3:    liftA3,
  once:      once
}

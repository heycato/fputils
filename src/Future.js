const { stringify, once } = require('./utils')

const tryCatch = (rej, f) => (...args) => {
  try {
    return f.apply(null, args)
  } catch(e) {
    return rej(e)
  }
}

const Future = fork => ({
  fork,
  map: (f) =>
    Future((rej, res) =>
      fork((e) => rej(e),
           tryCatch(rej, (x) => Future.of(f(x)).fork(rej, res)))),
  chain: (f) =>
    Future((rej, res) =>
      fork((e) => rej(e),
           tryCatch(rej, (x) => f(x).fork(rej, res)))),
  ap: (m) =>
    Future((rej, res) => {
      let applyFn, val
      const doReject = once(rej)
      const resolveIfDone = tryCatch(rej, () =>
        applyFn != null &&
        val != null     &&
        res(applyFn(val)))
      fork(doReject, f => (applyFn = f) && resolveIfDone())
      m.fork(doReject, v => (val = v) && resolveIfDone())
    }),
  inspect:() => `Future(${stringify(fork)})`,
  __type__: 'Future'
})

Future.of = (x) => Future((_, res) => res(x))
Future.reject = (x) => Future((rej) => rej(x))

module.exports = Future

const { stringify } = require('./utils')

const Right = x => ({
  map: f => Right(f(x)),
  chain: f => f(x),
  fold: f => f(x),
  bifold: (_, f) => f(x),
  ap: e => e.map(x),
  concat: o =>
    o.bifold(e => Left(e),
             r => Right(x.concat(r))),
  inspect: () => `Right(${stringify(x)})`,
  __type__:'Right'
})

const Left = x => ({
  map: f => Left(x),
  chain: f => Left(x),
  fold: f => f(x),
  bifold: (f, _) => f(x),
  ap: e => Left(x),
  concat: o => Left(x),
  inspect: () => `Left(${stringify(x)})`,
  __type__:'Left'
})

const Either = x =>
  x !== null ? Right(x) : Left(null)

Either.of = Right
Either.try = (f) => (...args) => {
  try {
    return Right(f.apply(null, args))
  } catch(e) {
    return Left(e)
  }
}
Either.Right = Right
Either.Left = Left

module.exports = Either

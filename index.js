module.exports = {
  Either: require('./src/Either'),
  Future: require('./src/Future'),
  Maybe: require('./src/Maybe'),
  IO: require('./src/IO'),
  utils: require('./src/utils')
}

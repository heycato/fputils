const {
  Either,
  Future,
  Maybe,
  IO,
  utils
} = require('../index.js')

const {
  stringify,
  getType,
  liftA2,
  liftA3,
  once
} = utils

const id = x => x

describe('stringify', () => {

  const tests = [
    [() => {}, '() => {}'],
    [{one:1}, '{\"one\":1}'],
    [[1,2,3,4,5], '[1,2,3,4,5]'],
    [/(?:)/, '/(?:)/'],
    [105, '105'],
    ['hi', 'hi']
  ]
  .map(([o, res]) => {
    test(`${res} to human readable string`, () => {
      expect(stringify(o)).toBe(res)
    })
  })

})

describe('getType', () => {

  class Custom { constructor() {} }

  const tests = [
    [undefined, 'Undefined'],
    [, 'Undefined'],
    [null, 'Null'],
    [{}, 'Object'],
    [[], 'Array'],
    [0, 'Number'],
    [100.234, 'Number'],
    ["Hi", 'String'],
    [() => {}, 'Function'],
    [/(?:)/, 'RegExp'],
    [new Custom(), 'Custom']
  ]
  .map(([o, res]) => {
    test(`is ${res}`, () => {
      expect(getType(o)).toBe(res)
    })
  })

})

describe('once', () => {

  const incOnce = once(n => n + 1)

  test('runs function once', () => {
    expect(incOnce(1)).toBe(2) 
  })

  test('and only once', () => {
    expect(incOnce(5)).toBe(false)
  })

})

describe('lift helpers', () => {

  const add2 = x => y => x + y
  const add3 = x => y => z => x + y + z
  const fFive = Future.of(5)
  const fSix = Future.of(6)
  const fSeven = Future.of(7)

  const ioOne = IO.of(1)
  const ioTwo = IO.of(2)

  test('liftA2 applies two Monads of value to a curried function', () => {
    expect(liftA2(add2, ioOne, ioTwo).run()).toBe(3)
  })

  test('liftA3 applies three Monads of value to a curried function', () => {
    liftA3(add2, fFive, fSix, fSeven)
      .fork(id, n => {
        expect(n).toBe(18)
        done()
      })
  })

})

describe('Either', () => {

  const rInc = Either.Right(n => n + 1)
  const rFive = Either.Right(5)
  const rSix = Either.Right(6)
  const left = Either.Left('Some error')

  test('returns a Right when value not null', () => {
    expect(getType(Either(5))).toBe('Right')
  })

  test('returns a Left when value is null', () => {
    expect(getType(Either(null))).toBe('Left')
  })

  test('pointed functor returns Right', () => {
    expect(getType(Either.of(null))).toBe('Right')
    expect(getType(Either.of('anything'))).toBe('Right')
  })

  test('Either.try returns Right on success', () => {
    const success = Either.try((n) => n + 1)
    expect(getType(success(2))).toBe('Right')
  })

  test('Either.try returns Left on fail', () => {
    const fail = Either.try((n) => {
      throw new Error('fail')
    })
    expect(getType(fail(2))).toBe('Left')
  })

  test('Right.map applies function to value', () => {
    expect(rFive.map(n => n + 1).inspect()).toBe('Right(6)')
  })

  test('Left.map does not apply function to value', () => {
    expect(left.map(m => m + ' about something').inspect()).toBe('Left(Some error)')
  })

  test('Right.fold returns function applied to value', () => {
    expect(rFive.fold(n => n + 1)).toBe(6)
  })

  test('Left.fold returns function applied to value', () => {
    expect(left.fold(x => x + '!')).toBe('Some error!')
  })

  test('Right.bifold returns second function applied to value', () => {
    expect(rFive.bifold(x => 'hi', n => n + 1)).toBe(6)
  })

  test('Left.bifold returns value, ignoring both functions', () => {
    expect(left.bifold(n => 'hi', x => 'world')).toBe('hi')
  })

  test('Right.chain expects function to return an Either', () => {
    expect(rFive.chain(Either).inspect()).toBe('Right(5)')
  })

  test('Left.chain returns itself', () => {
    expect(left.chain(Either).inspect()).toBe('Left(Some error)')
  })

  test('Right.ap applies fn to functor of value', () => {
    expect(rInc.ap(rFive).inspect()).toBe('Right(6)')
  })

  test('Left.ap returns itself', () => {
    expect(left.ap(rFive).inspect()).toBe('Left(Some error)')
  })

  test('Right.concat concats two semi-groups', () => {
    const rStr1 = Either.Right('Hello ')
    const rStr2 = Either.Right('World')
    expect(rStr1.concat(rStr2).inspect()).toBe('Right(Hello World)')
  })

  test('Right.concat with a Left returns the Left', () => {
    const rStr1 = Either.Right('Hello ')
    expect(rStr1.concat(left).inspect()).toBe('Left(Some error)')
  })

  test('Left.concat returns itself', () => {
    expect(left.concat(rFive).inspect()).toBe('Left(Some error)')
  })

  test('Either.inspect returns a String', () => {
    expect(getType(Either.of(5).inspect())).toBe('String')
  })

})

describe('Future', () => {
const log = x => {
  console.log(x.toString())
  return x
}

  const fHello = Future.of('hello')
  const fWorld = Future.of((hi) => `${hi} world`)

  test('pointed functor returns IO', () => {
    expect(getType(Future.of('anything'))).toBe('Future')
  })

  test('Future.map composition', () => {
    expect(Future.of(5).map(n => n + 1).fork(id, id)).toBe(6)
  })

  test('Future.chain expects function to return an Future', () => {
    expect(Future.of(5).chain(Future.of).fork(id, id)).toBe(5)
  })

  test('Future.ap applies fn to functor of value', (done) => {
    fWorld
      .ap(fHello)
      .fork(id, (x) => {
        expect(x).toBe('hello world')
        done()
      })
  })

  test('Future.ap rejects on error in first', (done) => {
    Future.of(x => { throw new Error('Broke') })
      .ap(Future.of(1))
      .fork((err) => {
        expect(err.toString()).toBe('Error: Broke')
        done()
      }, id)
  })

  test('Future.chain expects function to return an Future', (done) => {
    Future
      .reject(5)
      .fork(x => {
        expect(x).toBe(5)
        done()
      })
  })

  test('Future.fork applies the computation', (done) => {
    Future.of(5)
      .fork(id, n => {
        expect(n).toBe(5)
        done()
      })
  })

  test('Future.inspect returns a String', () => {
    expect(getType(Future.of(5).inspect())).toBe('String')
  })

})

describe('Maybe', () => {

  const mFive = Maybe.Some(5)
  const mInc = Maybe.Some(x => x + 1)
  const none = Maybe.None()

  test('returns a Some when value not null', () => {
    expect(getType(Maybe(5))).toBe('Some')
  })

  test('returns a None when value is null', () => {
    expect(getType(Maybe(null))).toBe('None')
  })

  test('pointed functor returns Some', () => {
    expect(getType(Maybe.of(null))).toBe('Some')
    expect(getType(Maybe.of('anything'))).toBe('Some')
  })

  test('Some.map applies function to value', () => {
    expect(mFive.map(n => n + 1).inspect()).toBe('Some(6)')
  })

  test('None.map returns None', () => {
    expect(none.map(n => n + 1).inspect()).toBe('None()')
  })

  test('Some.fold returns function applied to value', () => {
    expect(mFive.fold(n => n + 1)).toBe(6)
  })

  test('None.fold returns null', () => {
    expect(none.fold(n => n + 1)).toBe(null)
  })

  test('Some.chain expects function to return a Maybe', () => {
    expect(mFive.chain(Maybe).inspect()).toBe('Some(5)')
  })

  test('None.chain expects function to return a Maybe', () => {
    expect(none.chain(Maybe).inspect()).toBe('None()')
  })

  test('Some.ap applies fn to functor of value', () => {
    expect(mInc.ap(mFive).inspect()).toBe('Some(6)')
  })

  test('Some.ap applied to None returns None', () => {
    expect(mInc.ap(none).inspect()).toBe('None()')
  })

  test('None.ap returns itself', () => {
    expect(none.ap(mFive).inspect()).toBe('None()')
  })

  test('Maybe.inspect returns a String', () => {
    expect(getType(Maybe.of(5).inspect())).toBe('String')
  })

})

describe('IO', () => {
  const ioHi = IO(() => 'hi')
  const world = (x) => `${x} world`
  const ioHello = IO(() => 'hello')
  const ioWorld = IO.of(world)

  test('pointed functor returns IO', () => {
    expect(getType(IO.of('anything'))).toBe('IO')
  })

  test('IO.map composition', () => {
    expect(ioHi.map(world).run()).toBe('hi world')
  })

  test('IO.chain expects function to return an IO', () => {
    expect(IO.of(5).chain(IO.of).run()).toBe(5)
  })

  test('IO.ap applies fn to functor of value', () => {
    expect(ioWorld.ap(ioHello).run()).toBe('hello world')
  })

  test('IO.run applies the computation', () => {
    expect(IO.of(5).run()).toBe(5)
  })

  test('IO.inspect returns a String', () => {
    expect(getType(IO.of(5).inspect())).toBe('String')
  })
})
